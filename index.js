const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const request = require('request-promise').defaults({jar: true});

console.log('Loading Data, Please Wait!');
(async ()=>
{
    const browser = await puppeteer.launch({
        headless: false
    });
    const page = await browser.newPage();
    page.setUserAgent('Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1');
    await page.goto('https://www.scubaearth.com/default.aspx', {
        waitUntil: 'load', timeout: 0
    });
    await page.type('#txtUserName', 'herewriteyouremail')
    await page.type('#txtPassword', 'herewriteyourpassword');
    await Promise.all([
        page.waitForNavigation(),
        page.click('#btnLogin')
    ]);
    await page.goto('http://www.scubaearth.com/dive-shop/dive-shop-info-display.aspx?affiliateid=12582', 
    {
        waitUntil: 'load', timeout: 0
    });

    console.log('Webpage Target Loaded Success!');
    try{
        await page.waitForSelector('title');
        const element = await page.$('title');
        const webpagetitle = await page.evaluate(()=> document.querySelector('title').textContent);
        const namesite = await page.evaluate(()=>document.getElementById('ctl00_ctl00_ctl00_cphMain_cphMiddle_cphLeftColumn_widgetDiveShopProfile_hlDiveShopName').textContent);
        const divercountry = await page.evaluate(()=> document.getElementById('ctl00_ctl00_ctl00_cphMain_cphMiddle_cphCenterColumn_uctDiveShopBar_lblDiverCountry').textContent);
        const phone = await page.evaluate(()=> document.getElementById('ctl00_ctl00_ctl00_cphMain_cphMiddle_cphCenterColumn_uctDiveInfoDisplay_lblStorePhone').textContent);
        const email =await page.evaluate(()=> document.getElementById('ctl00_ctl00_ctl00_cphMain_cphMiddle_cphCenterColumn_uctDiveInfoDisplay_hlStoreEmail').textContent);
        const website = await page.evaluate(()=> document.getElementById('ctl00_ctl00_ctl00_cphMain_cphMiddle_cphCenterColumn_uctDiveInfoDisplay_hlStoreWeb').textContent);
        const shopanddaysandhour = await page.evaluate(()=> document.getElementById('ctl00_ctl00_ctl00_cphMain_cphMiddle_cphCenterColumn_uctDiveInfoDisplay_TabContainer1_tabOperations_ShopHoursViewDIV').textContent);
        
        console.log('');
        console.log('webpageTitle: ', webpagetitle.replace(/\s\s+/g, ' '));
        console.log('WebPageURL: ', page.url());
        console.log('namesite: ', namesite);
        console.log('divercountry: ', divercountry);
        console.log('phone: ', phone);
        console.log('email: ', email);
        console.log('website: ', website);
        console.log('shopAndDaysAndHours: ', shopanddaysandhour);

    }catch(e)
    {
        console.log('error: ', e);
    }
})();
